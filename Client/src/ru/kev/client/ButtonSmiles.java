package ru.kev.client;

import javax.swing.*;
import java.awt.*;

/**
 * Класс для создания диалогового окна с кнопками и вывода графического изображения в чат
 *
 * @author Kotelnikova E.V. group 15it20
 */

class ButtonSmiles extends JFrame {
    private static final int WIDTH = 200;
    private static final int HEIGHT = 100;

    ButtonSmiles() {
        JDialog dialog = new JDialog(this, "Emoji", true);
        dialog.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        dialog.setSize(200, 90);
        JPanel panel = new JPanel();

        JButton laugh = smileAction("D:\\Work\\System Programming\\MyChat\\Client\\laugh.png", 0x1F603);
        JButton cry = smileAction("D:\\Work\\System Programming\\MyChat\\Client\\cry.png", 0x1F62D);
        JButton angry = smileAction("D:\\Work\\System Programming\\MyChat\\Client\\angry.png", 0x1F621);

        panel.add(laugh);
        panel.add(cry);
        panel.add(angry);
        setContentPane(panel);

        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        setSize(WIDTH, HEIGHT);
        setLocationRelativeTo(null);
        setAlwaysOnTop(true);
        setResizable(false);
    }

    /**
     * Метод для определения действий кнопок
     *
     * @param path путь к изображению кнопки
     * @param unicode юникод эмоджи
     * @return кнопка
     */
    private JButton smileAction(String path, int unicode){
        JButton button = new JButton(new ImageIcon(path));
        button.setPreferredSize(new Dimension(40,40));
        button.addActionListener(e -> {
            ClientWindow.getConnection().sendString(ClientWindow.getFieldNickname().getText() + ": " + getEmojiByUnicode(unicode));
            //textView.setText(emoji);
        });
        return button;
    }

    void open() {
        setVisible(true);
    }

    /**
     * Метод для преобразования юникода
     * @param unicode юникод эмоджи
     * @return преобразованный юникод
     */
    private String getEmojiByUnicode(int unicode) {
        return new String(Character.toChars(unicode));
    }

//
//    public IB createSmile(JTextArea dialog, String pathName) {
//        IB smile = null;
//        try {
//            Image img = (ImageIO.read(new File(pathName)));
//            smile = new IB(img);
//            smile.update(dialog.getGraphics());
//            smile.setPreferredSize(new Dimension(10, 10));
//            dialog.add(smile);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return smile;
//    }
}
